
import {createAppContainer}   from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

// import Login         from './pages/Login';
// import Signup        from './pages/Signup';
import Home          from './pages/Home';
import Plans         from './pages/Plans';
// import WeeklyPlan    from './pages/WeeklyPlan';
// import DailySummary  from './pages/DailySummary';
// import ChosenProgram from './pages/ChosenProgram';
// import Settings      from './pages/Settings';

const MainNavigator = createStackNavigator(
	{
		Home: {screen: Home},
		Plans: {screen: Plans},
		// Signup: {screen: Signup},
		// Programs: {screen: Programs},
		// WeeklyPlan: {screen: WeeklyPlan},
		// DailySummary: {screen: DailySummary},
		// ChosenProgram: {screen: ChosenProgram},
		// Settings: {screen: Settings},
	},
	{
		initialRouteName: 'Home',
	}
);

const App = createAppContainer(MainNavigator);

export default App;