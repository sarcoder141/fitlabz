import {Dimensions} from 'react-native';

const screenWidth  = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

global['sw'] = function (percent) {return (screenWidth / 100)  * percent}
global['sh'] = function (percent) {return (screenHeight / 100) * percent}