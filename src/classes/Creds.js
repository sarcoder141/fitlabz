
import * as Keychain from 'react-native-keychain';

module.exports = class Creds 
{
	static async storeCreds(username, password)
	{
		if (!username || typeof username === 'undefined' || username == '' || username == null) return false;
		if (!password || typeof password === 'undefined' || password == '' || password == null) return false;

		try
		{
			await Keychain.setGenericPassword(username, password);

			return true;
		}
		catch (e)
		{
			console.log('ERROR STORING CREDENTIALS: ', e.message);

			return false;
		}
	}

	static async getCredentials()
	{
		try 
		{
			// Retreive the credentials
			const credentials = await Keychain.getGenericPassword();

			if (!credentials) 
			{
				false;
			} 
			
			if (!credentials.username) return false;
			if (!credentials.password) return false;

			return {
				email: credentials.username,
				pass: credentials.password
			}
		} 
		catch (error) 
		{
			console.log('Keychain couldn\'t be accessed!', error);

			return false;
		}
	}

	static async removeCredentials()
	{
		try
		{
			await Keychain.resetGenericPassword();

			return true;
		}
		catch (e)
		{
			console.log('ERROR DELETING CREDENTIALS: ', e.message);

			return false;
		}
	}
}