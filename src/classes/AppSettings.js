import {AsyncStorage} from 'react-native';

export default class AppSettings
{
	static settings = {};

	static async initMainSettings()
	{
		try
		{
			let value = await AsyncStorage.getItem('settings');

			if (value !== null)
			{
				this.settings = JSON.parse(value);
			}

			// console.log(' -------------------------------------------- ');
			// console.log('                                   ');
			// console.log('                                   ');
			// console.log('SETTINGS HERE: ', value);
		}
		catch (error) {this.settings = {}; console.log('ERROR GETTING SETTINGS: ', error);}
	}

	static async clear()
	{
		await AsyncStorage.clear();

		this.settings = {};

		await this.persistSettings();
	}

	static async set(key, value)
	{
		this.settings[key] = value;

		try
		{
			await AsyncStorage.setItem(
				'settings',
				JSON.stringify(this.settings)
			);
		}
		catch (error)
		{
			console.log('Error setting settings var: ', error);
		}
	}

	static get(key, defaultValue = '')
	{
		if (!this.hasKey(key))
		{
			return defaultValue;
		}

		return this.settings[key];
	}

	static async persistSettings()
	{
		try
		{
			await AsyncStorage.setItem(
				'settings',
				JSON.stringify(this.settings)
			);
		}
		catch (error)
		{
			console.log('Error setting settings var: ', error);
		}
	}

	static getSettings()
	{
		return this.settings;
	}

	static hasKey(key) {return typeof this.settings[key] !== 'undefined'}

	static hasCreatedKeys()
	{
		if (typeof this.settings['hasKey'] === 'undefined') return false;

		return this.settings['hasKey'];
	}
}