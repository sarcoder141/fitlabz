module.exports = class EventsClass {

	constructor ()
	{
		this.events = {};
	}

	on (eventName, func, context)
	{
		this.events[eventName] = this.events[eventName] || [];
		this.events[eventName].push({func});
	}

	off (eventName, func)
	{
		if (this.events[eventName])
		{
			if (func == undefined)
			{
				delete this.events[eventName];
				return;
			}

			for (var i = 0; i < this.events[eventName].length; i++)
			{
				if (this.events[eventName][i] === func)
				{
					this.events[eventName].splice(i, 1);
					break;
				}
			};
		}
	}

	emit (eventName, data)
	{
		if (this.events[eventName])
		{
			if (this.events[eventName].length === undefined)
			{
				console.log('There was a problem with the event name : ' + eventName);
				return;
			}

			for (var i = 0; i < this.events[eventName].length; i++)
			{
				var item = this.events[eventName][i];

				if (this._isFunction(item['func']))
				{
					item['func'](data);
					continue;
				}
			}
		}
	}

	_isFunction (functionToCheck)
	{
		var getType = {};

		return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
	}

	eventsCount ()
	{
		return Object.keys(this.events).length;
	}

	listEvents (eventName)
	{
		if (eventName === undefined)
		{
			for (var key in this.events)
			{
				console.log('Key : ' + key + ' : ' + this.events[key]);
			}
		}

		if (this.events[eventName] === undefined) {
			console.log('Your event doesn\'t exist');
			return;
		}

		console.log(' ------------------------------------------------------ CURRENT EVENTS LIST -----------------------------------------------------------------------------');

		for (var key in this.events[eventName])
		{
			console.log('Key : ' + key + ' : ' + this.events[eventName][key]);
		}
	}
}