
module.exports = class Request
{
	static doRequest(url, method, params, successCb)
	{
		return new Promise(
			(resolve, reject) =>
			{
				if (typeof params !== 'object') params = [];

				//console.clear();

				const Http = new XMLHttpRequest();

				Http.open(method, url, true);
				Http.setRequestHeader('Content-type', 'application/json');

				if (Object.keys(params).length > 0)
				{
					Http.send(JSON.stringify(params));
				}
				else
				{
					Http.send();
				}

				Http.onreadystatechange = function (e)
				{
					if (this.readyState == 4)
					{
						if (typeof successCb == 'function')
						{
							successCb(Http.responseText);
						}

						resolve(Http.responseText);
					}
				}

				Http.onerror = function ()
				{
					reject();
				}
			}
		);
	}

	static post(url, params, successCb) {return Request.doRequest(url, 'POST', params, successCb)}
}