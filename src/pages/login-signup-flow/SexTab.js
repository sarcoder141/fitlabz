
import React, {Component} from 'react';

import {View, StyleSheet, TouchableOpacity} from 'react-native';

import {Text} from '@ui-kitten/components';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

function getStyles()
{
    let btnShadowMA = 7;

    return {
        bold: 
        {
            fontWeight: 'bold'
        },
        explan:
        {
            marginTop: 10,
        },
        cText: 
        {
            textAlign: 'center',
        },
        contTab:
        {
            width: sw(70),
            height: '100%',
            paddingTop: 20,
            alignItems: 'center',
            justifyContent: 'center'
        },
        btnCont:
        {
            width: '100%', 
            flexDirection: 'row', 
            justifyContent: 'space-evenly',
            alignItems: 'center',
            marginTop: 30,
        },
        sexBtnCont:
        {
            width: 100,
            height: 100,
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 25,
            marginRight: 25,
        },
        icon:
        {
            fontSize: 55,
        },
        btnShadow:
        {
            flex: 1,
            borderRadius: 10,
            position: 'absolute',
            left: -(btnShadowMA),
            right: -(btnShadowMA),
            top: -(btnShadowMA),
            bottom: -(btnShadowMA),
            shadowColor: 'black',
            shadowOffset: { width: .2, height: .2 },
            shadowOpacity: 0.8,
            shadowRadius: 7,
            elevation: 6.5,
        },
        btnBg:
        {
            borderColor: "rgba(0, 0, 0, 0.1)",
            backgroundColor: 'white',
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            borderRadius: 10,
        }
	};
}

const styles = StyleSheet.create(getStyles());

export default class SexTab extends Component {
  
    constructor(props)
    {
        super(props);

        this.state = {
            sex: '',
        };
    }

    sexIsValid()
    {
        return /[0-9]{1}/.test(this.state.sex);
    }

    setSex(id)
    {
        this.setState({sex: id});

        setTimeout(() => this.props.nextTab(), 350);
    }

    render()
    {
        return (
            <View style={styles.contTab}>

                <View style={styles.btnCont}>

                    <TouchableOpacity 
                        style={styles.sexBtnCont}
                        onPress={() => this.setSex(0)}
                    >

                        <View style={styles.btnShadow}/>

                        <View style={styles.btnBg}/>

                        <View style={[styles.btnBg, {backgroundColor: 'white'}]}/>

                        <Icon
                            style={styles.icon}
                            color={this.state.sex == 0 ? '#41CAF7' : '#E4E4E9'}
                            name="gender-male"
                        />

                    </TouchableOpacity>

                    <TouchableOpacity 
                        style={styles.sexBtnCont}
                        onPress={() => this.setSex(1)}
                    >

                        <View style={styles.btnShadow}/>

                        <View style={styles.btnBg}/>

                        <Icon
                            style={styles.icon}
                            color={this.state.sex == 1 ? '#FF78FC' : '#E4E4E9'}
                            name="gender-female"
                        />

                    </TouchableOpacity>

                </View>

                <View style={[styles.btnCont, {marginBottom: 55}]}>

                    <TouchableOpacity 
                        style={styles.sexBtnCont}
                        onPress={() => this.setSex(2)}
                    >

                        <View style={styles.btnShadow}/>

                        <View style={styles.btnBg}/>

                        <Icon
                            style={styles.icon}
                            color={this.state.sex == 2 ? '#E600E6' : '#E4E4E9'}
                            name="gender-male-female"
                        />

                    </TouchableOpacity>

                </View>

                <Text category='h6' style={styles.bold}>
                    
                    Please choose your sex?

                </Text>

                <Text category='p1' style={[styles.explan, styles.cText]}>
                    
                    This is so we can best match our exercise content to your desired goals.

                </Text>

                <Text category='p1' style={styles.cText}>
                    
                    Thank you

                </Text>

            </View>
        );
    }
};