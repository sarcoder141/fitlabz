
import React, {Component} from 'react';

import {View, StyleSheet} from 'react-native';

import {Button, Text} from '@ui-kitten/components';

import TextInput from '../../components/TextInput';

function getStyles()
{
    return {
        bold: 
        {
            fontWeight: 'bold'
        },
        explan:
        {
            marginTop: 10,
        },
        cText: 
        {
            textAlign: 'center',
        },
        contTab:
        {
            width: sw(70),
            height: '100%',
            paddingTop: 20,
            alignItems: 'center',
            justifyContent: 'center'
        },
        btnCont:
        {
            marginBottom: 70, 
            width: '100%', 
            flexDirection: 'row', 
            justifyContent: 'flex-end',
            marginTop: 20,
        }
	};
}

const styles = StyleSheet.create(getStyles());

export default class HeightTab extends Component {
  
    constructor(props)
    {
        super(props);

        this.state = {height: ''};
    }

    weightIsValid() {return /[0-9]+/.test(this.state.height.trim())}

    render()
    {
        return (
            <View style={styles.contTab}>

                <TextInput
                    parent={this}
                    bModel='height'
                    label='Height'
                    placeholder='Please enter an weight'
                    caption='The value must be a number'
                />

                <View style={styles.btnCont}>

                    <Button 
                        style={
                            [
                                styles.button,
                                {
                                    width: '40%'
                                }
                            ]
                        }
                        disabled={!this.weightIsValid()}
                        status='primary'
                        onPress={
                            () =>
                            {
                                if (!this.weightIsValid()) return;

                                this.props.nextTab();
                            }
                        }
                    >

                        NEXT

                    </Button>

                </View>

                <Text category='h6' style={styles.bold}>
                    
                    What's your weight?

                </Text>

                <Text category='p1' style={[styles.explan, styles.cText]}>
                    
                    Please supply a height to help us best advise on proper plans for your body type.

                </Text>

                <Text category='p1' style={styles.cText}>
                    
                    Thank you

                </Text>

            </View>
        );
    }
};