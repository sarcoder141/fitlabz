import React, {Component} from 'react';

import {View, StyleSheet, Keyboard} from 'react-native';

import {Button, Icon, Text, Spinner} from '@ui-kitten/components';

import TextInput from '../../components/TextInput';

function getStyles()
{
    let lrMargin = 27;

	return {
		btnColumn:
		{
			width: '100%',
			flexDirection: 'column',
		},
		button:
		{
			marginLeft: lrMargin,
			marginRight: lrMargin,
		},
		inputCont:
		{
			width: '70%',
			paddingTop: 80
		},
		input: 
		{
			marginVertical: 2,
        },
        fbBtn:
		{
			flex: 1,
			backgroundColor: '#4c669f'
		},
		txtOr:
		{
			marginTop: 10,
			marginBottom: 10,
			textAlign: 'center',
			fontSize: 10,
		},
		btnLogin:
		{
			flex: 1,
			marginTop: 25,
		},
		txtForgotPass:
		{
			marginTop: 10,
			marginBottom: 10,
			textAlign: 'center',
			fontSize: 12,
        },
        indicator:
        {
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute'
        },
	};
}

const styles = StyleSheet.create(getStyles());

export default class LoginCont extends Component {
  
    constructor(props)
    {
        super(props);

        this.state = {
            username: '',
            password: '',

            requestInProgress: false,
        };
    }

    render()
    {
        const LoadingIndicator = (props) => (
            <View style={[props.style, styles.indicator]}>

              <Spinner size='small'/>

            </View>
        );
            
        return (
            <View style={styles.inputCont}>

                <Button 
                    style={styles.fbBtn}
                    accessoryLeft={
                        (props) => (
                            <Icon {...props} name='facebook' />
                        )
                    }
                >
                    
                    LOGIN WITH FACEBOOK

                </Button>

                <Text style={styles.txtOr} category='p1'>Or</Text>

                <TextInput
                    parent={this}
                    bModel='username'
                    label='Username'
                    placeholder='Place a Useranme'
                />

                <TextInput
                    parent={this}
                    bModel='password'
                    label='Password'
                    placeholder='Place your password'
                    caption='Should contain at least 8 symbols'
                    isSecureText={true}
                />

                <Button 
                    style={styles.btnLogin}
                    status='basic'
                    accessoryRight={this.state.requestInProgress ? LoadingIndicator : undefined}
                    onPress={
                        () =>
                        {
                            Keyboard.dismiss();

                            this.setState({requestInProgress: true});

                            setTimeout(
                                () =>
                                {
                                    this.props.navi.replace('Plans');
                                },
                                1300
                            );
                        }
                    }
                >

                    LOGIN

                </Button>

                <Text style={styles.txtForgotPass} category='p1'>
                    
                    Forgot password?

                </Text>

            </View>
        );
    }
};