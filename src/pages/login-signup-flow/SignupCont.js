import React, {Component} from 'react';

import {View, StyleSheet, Animated, Easing, Keyboard, Text} from 'react-native';

import SignupTab from './SignupTab';
import SexTab    from './SexTab';
import AgeTab    from './AgeTab';
import HeightTab from './HeightTab';
import WeightTab from './WeightTab';

function getStyles()
{
    let bulletSize = 9.5;

	return {
		signUpPageCont:
		{
            width: sw(70),
            paddingTop: 80,
            height: '100%',
            overflow: 'hidden',
        },
        tabsCont:
        {
            width: sw(280),
            height: '100%',
            position: 'absolute',
            flexDirection: 'row',
            justifyContent: 'space-between',
            top: 0,
            bottom: 0,
        },
        bulletIndicatorsCont:
        {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            position: 'absolute',
            bottom: 60,
            left: 0,
            right: 0,
        },
        bullet:
        {
            width: bulletSize,
            height: bulletSize,
            borderRadius: bulletSize / 2,
            backgroundColor: '#DCDCDD',
            marginLeft: 3, 
            marginRight: 3,
        },
        active:
        {
            backgroundColor: '#8D8D91'
        }
	};
}

const styles = StyleSheet.create(getStyles());

export default class SignupCont extends Component {
  
    constructor(props)
    {
        super(props);
        
        this.state = {
            tabsLeft: new Animated.Value(0),
            left: 0,

            currentTabIndex: 0,
        };

        this.props.events.on(
            'title-clicked',
            () =>
            {
                this.switchTabPos(1);
            }
        );
    }

    currentTab() {return this.state.currentTabIndex}

    previousTab()
	{
        if (this.state.currentTabIndex == 0) return;

        this.setState({currentTabIndex: this.state.currentTabIndex--});

        this.switchTabPos(this.state.currentTabIndex);
	}

	nextTab()
	{
		if (this.state.currentTabIndex > 3) return;

        Keyboard.dismiss();

        this.switchTabPos(this.state.currentTabIndex + 1);
	}

    switchTabPos(index)
    {
        this.setState({currentTabIndex: index});

        this.props.onIndexChanged(index);

        if (index <= 1)
        {
            this.props.events.emit('default-gradient-hide-back');
        }
        else if (index > 1 && index <= 3)
        {
            this.props.events.emit('default-gradient-show-back');
        }

        Animated.timing(
            this.state.tabsLeft,
            {
                toValue: -(sw(70) * index),
                duration: 200,
                easing: Easing.in(),
            }
        ).start();
    }

    render()
    {
        return (
            <View style={styles.signUpPageCont}>

                <Animated.View 
                    style={
                        [
                            styles.tabsCont,
                            {
                                left: this.state.tabsLeft,
                            }
                        ]
                    }
                >

                    <SignupTab 
                        events={this.props.events}
                        nextTab={this.nextTab.bind(this)}
                    />
                       
                    <SexTab 
                        nextTab={this.nextTab.bind(this)}
                    />

                    <AgeTab 
                        nextTab={this.nextTab.bind(this)}
                    />

                    <HeightTab 
                        nextTab={this.nextTab.bind(this)}
                    />

                    <WeightTab 
                        nextTab={this.nextTab.bind(this)}
                        navi={this.props.navi}
                    />

                </Animated.View>

                {this.state.currentTabIndex > 0 ? <View style={styles.bulletIndicatorsCont}>

                    <View 
                        style={
                            [
                                styles.bullet,
                                this.state.currentTabIndex == 1 ? styles.active : undefined
                            ]
                        } 
                    />

                    <View 
                        style={
                            [
                                styles.bullet,
                                this.state.currentTabIndex == 2 ? styles.active : undefined
                            ]
                        } 
                    />

                    <View 
                        style={
                            [
                                styles.bullet,
                                this.state.currentTabIndex == 3 ? styles.active : undefined
                            ]
                        } 
                    />

                    <View 
                        style={
                            [
                                styles.bullet,
                                this.state.currentTabIndex == 4 ? styles.active : undefined
                            ]
                        } 
                    />

                </View> : undefined}

            </View>
        );
    }
};