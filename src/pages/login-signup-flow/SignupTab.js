
import React, {Component} from 'react';

import {View, StyleSheet} from 'react-native';

import {Button, Icon, Text, Spinner} from '@ui-kitten/components';

import TextInput from '../../components/TextInput';

function getStyles()
{
    let lrMargin = 27;

	return {
		inputCont:
		{
            width: sw(70),
            paddingTop: 80,
            height: '100%',	
        },
        fbBtn:
		{
			flex: 1,
			backgroundColor: '#4c669f'
		},
		txtOr:
		{
			marginTop: 10,
			marginBottom: 10,
			textAlign: 'center',
			fontSize: 10,
		},
		btnSignup:
		{
			flex: 1,
			marginTop: 25,
		},
		txtForgotPass:
		{
			marginTop: 10,
			marginBottom: 10,
			textAlign: 'center',
			fontSize: 12,
        },
        indicator: 
        {
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute'
        },
        contTab:
        {
            width: sw(70),
            height: '100%',
            paddingTop: 80,
        }
	};
}

const styles = StyleSheet.create(getStyles());

export default class SignupTab extends Component {
  
    constructor(props)
    {
        super(props);

        this.state = {
            username: 'theguy',
            password: 'Cooke',
            firstName: 'Ryan',
            lastName: 'Cooke',
            requestInProgress: false,
        };
    }

    signupIsValid()
    {
        if (this.state.requestInProgress) return false;

        if (this.state.username  == '') return false;
        if (this.state.password  == '') return false;
        if (this.state.firstName == '') return false;
        if (this.state.lastName  == '') return false;

        return true;
    }

    async signupSuccessful()
    {
        this.setState({requestInProgress: true});

        setTimeout(() => this.props.nextTab(), 1000);
    }

    render()
    {
        const LoadingIndicator = (props) => (
            <View style={[props.style, styles.indicator]}>

              <Spinner size='small'/>

            </View>
        );

        return (
            <View style={styles.contTab}>

                <View>

                    <Button 
                        style={styles.fbBtn}
                        accessoryLeft={
                            (props) => (
                                <Icon {...props} name='facebook' />
                            )
                        }
                    >
                        
                        LOGIN WITH FACEBOOK

                    </Button>

                    <Text style={styles.txtOr} category='p1'>Or</Text>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>

                        <TextInput
                            parent={this}
                            bModel='firstName'
                            placeholder='First Name'
                        />

                        <TextInput
                            parent={this}
                            bModel='lastName'
                            placeholder='First Name'
                        />

                    </View>

                    <TextInput
                        parent={this}
                        bModel='username'
                        label='Username'
                        placeholder='Place a Useranme'
                    />

                    <TextInput
                        parent={this}
                        bModel='password'
                        label='Password'
                        placeholder='Place your password'
                        caption='Should contain at least 8 symbols'
                        isSecureText={true}
                    />

                    <Button 
                        style={styles.btnSignup}
                        disabled={!this.signupIsValid()}
                        status='primary'
                        accessoryRight={this.state.requestInProgress ? LoadingIndicator : undefined}
                        onPress={
                            () =>
                            {
                                this.signupSuccessful();
                            }
                        }
                    >

                        SIGNUP

                    </Button>

                </View>

            </View>
        );
    }
};