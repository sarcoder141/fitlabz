
import React, {Component} from 'react';

import {View, StyleSheet, Keyboard} from 'react-native';

import {Button, Text, Spinner} from '@ui-kitten/components';

import TextInput from '../../components/TextInput';

function getStyles()
{
    return {
        bold: 
        {
            fontWeight: 'bold'
        },
        explan:
        {
            marginTop: 10,
        },
        cText: 
        {
            textAlign: 'center',
        },
        contTab:
        {
            width: sw(70),
            height: '100%',
            paddingTop: 20,
            alignItems: 'center',
            justifyContent: 'center'
        },
        btnCont:
        {
            marginBottom: 70, 
            width: '100%', 
            flexDirection: 'row', 
            justifyContent: 'flex-end',
            marginTop: 20,
        },
        indicator: 
        {
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute'
        },
	};
}

const styles = StyleSheet.create(getStyles());

export default class WeightTab extends Component {
  
    constructor(props)
    {
        super(props);

        this.state = {
            weight: '',
            requestInProgress: false,
        };
    }

    weightIsValid() 
    {
        if (this.state.requestInProgress) return false;

        return /[0-9]+/.test(this.state.weight.trim());
    }

    render()
    {
        const LoadingIndicator = (props) => (
            <View style={[props.style, styles.indicator]}>

              <Spinner size='small'/>

            </View>
        );

        return (
            <View style={styles.contTab}>

                <TextInput
                    parent={this}
                    bModel='weight'
                    label='Weight'
                    placeholder='Please enter an weight'
                    caption='The value must be a number'
                />

                <View style={styles.btnCont}>

                    <Button 
                        style={
                            [
                                styles.button,
                                {
                                    width: '40%'
                                }
                            ]
                        }
                        disabled={!this.weightIsValid()}
                        status='primary'
                        accessoryRight={this.state.requestInProgress ? LoadingIndicator : undefined}
                        onPress={
                            () =>
                            {
                                if (!this.weightIsValid()) return;

                                Keyboard.dismiss();

                                this.setState({requestInProgress: true});

                                setTimeout(
                                    () =>
                                    {
                                        this.props.navi.replace('Plans');
                                    },
                                    1300
                                );
                            }
                        }
                    >

                        FINISH

                    </Button>

                </View>

                <Text category='h6' style={styles.bold}>
                    
                    What's your weight?

                </Text>

                <Text category='p1' style={[styles.explan, styles.cText]}>
                    
                    Please supply a wight to help us best advise on proper nutrition.

                </Text>

                <Text category='p1' style={styles.cText}>
                    
                    Thank you

                </Text>

            </View>
        );
    }
};