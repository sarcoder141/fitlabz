import React, {Component} from 'react';

import {StyleSheet, Animated, View} from 'react-native';

import {Button} from '@ui-kitten/components';

function getStyles()
{
    let lrMargin = 47;

	return {
        btnColumn:
		{
            flex: 1,
            width: '100%',
            height: '100%',
			flexDirection: 'column',
            backgroundColor: 'yellow',
		},
		button:
		{
			marginLeft: lrMargin,
            marginRight: lrMargin,
            opacity: 0,
		},
	};
}

export default class Lines extends Component
{
	constructor (props)
	{
		super(props);

        let mt = 60;

        this.state = {
            btn1MT: new Animated.Value(10),
			btn2MT: new Animated.Value(10),
			btn1O: new Animated.Value(0),
			btn2O: new Animated.Value(0),
        };

        this.startAnimations();
	}

    async startAnimations()
	{
		let aniSpeed = 300,
			marginTo = 30;
		
		setTimeout(
			() =>
			{
				Animated.parallel(
					[
						Animated.parallel(
							[
								Animated.timing(
									this.state.btn1MT, 
									{
										toValue: marginTo,
										duration: aniSpeed
									}
								),
								Animated.timing(
									this.state.btn1O, 
									{
										toValue: 1,
										duration: aniSpeed
									}
								),
							]
						),
		
						Animated.parallel(
							[
								Animated.timing(
									this.state.btn2MT, 
									{
										toValue: marginTo,
										duration: aniSpeed * 1.1
									}
								),
								Animated.timing(
									this.state.btn2O, 
									{
										toValue: 1,
										duration: aniSpeed * 1.1
									}
								),
							]
						),
					]
				).start();
			},
			350
		);
	}

	render()
	{
		return (
			<View style={{flex: 1}}>

                <Animated.View
                    style={
                        [
                            styles.button,
                            {
                                marginTop: this.state.btn1MT,
                                opacity: this.state.btn1O,
                            }
                        ]
                    } 
                >

                    <Button 
                        style={styles.button} 
                        status='basic'
                    >

                        LOGIN

                    </Button>

                </Animated.View>

                <Animated.View
                    style={
                        [
                            styles.button,
                            {
                                marginTop: 10
                            },
                            {
                                marginBottom: this.state.btn2MT,
                                opacity: this.state.btn2O,
                            }
                        ]
                    } 
                >

                    <Button 
                        style={styles.button} 
                        status='basic'
                    >

                        SIGNUP

                    </Button>

                </Animated.View>

            </View>
		);
	}
}

const styles = StyleSheet.create(getStyles());