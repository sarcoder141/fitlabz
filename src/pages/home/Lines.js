import React, {Component} from 'react';

import {StyleSheet, Animated, Easing} from 'react-native';

function getStyles()
{
	return {
        cont:
        {
            position: 'absolute',
            height: 300,
            left: 40,
            top: 220,
            opacity: 0.5
        },
        smallLine: 
        {
            backgroundColor: '#00FFFE',
            width: 50,
            height: 4,
            borderRadius: 2,
            opacity: 0,
        },
        mediumLine: 
        {
            backgroundColor: '#00FFFE',
            width: 75,
            height: 5.5,
            borderRadius: 6,
            opacity: 0,
        },
        largeLine: 
        {
            backgroundColor: '#00FFFE',
            width: 100,
            height: 7,
            borderRadius: 6,
            opacity: 0,
        }
	};
}

export default class Lines extends Component
{
	constructor (props)
	{
		super(props);

        let mt = 60;

        this.state = {

            slideLeft: new Animated.Value(40),

            line1: new Animated.Value(mt),
            line2: new Animated.Value(mt),
            line3: new Animated.Value(mt),
            line4: new Animated.Value(mt),
            line5: new Animated.Value(mt),
            line6: new Animated.Value(mt),

            line1O: new Animated.Value(0),
            line2O: new Animated.Value(0),
            line3O: new Animated.Value(0),
            line4O: new Animated.Value(0),
            line5O: new Animated.Value(0),
            line6O: new Animated.Value(0),
        };
	}

    async startAnimations()
    {
        let aniSpeed = 200;

        let marginTo = 40;

        setTimeout(
            () =>
            {
                Animated.parallel(
                    [
                        Animated.parallel(
                            [
                                Animated.timing(
                                    this.state.line1, 
                                    {
                                        toValue: marginTo,
                                        duration: aniSpeed,
                                        useNativeDriver: false,
                                    }
                                ),
                                Animated.timing(
                                    this.state.line1O, 
                                    {
                                        toValue: 1,
                                        duration: aniSpeed,
                                        useNativeDriver: false,
                                    }
                                ),
                            ]
                        ),

                        Animated.parallel(
                            [
                                Animated.timing(
                                    this.state.line2, 
                                    {
                                        toValue: marginTo,
                                        duration: aniSpeed * 1.1,
                                        useNativeDriver: false,
                                    }
                                ),
                                Animated.timing(
                                    this.state.line2O, 
                                    {
                                        toValue: 1,
                                        duration: aniSpeed * 1.1,
                                        useNativeDriver: false,
                                    }
                                ),
                            ]
                        ),

                        Animated.parallel(
                            [
                                Animated.timing(
                                    this.state.line3, 
                                    {
                                        toValue: marginTo,
                                        duration: aniSpeed * 1.2,
                                        useNativeDriver: false,
                                    }
                                ),
                                Animated.timing(
                                    this.state.line3O, 
                                    {
                                        toValue: 1,
                                        duration: aniSpeed * 1.2,
                                        useNativeDriver: false,
                                    }
                                ),
                            ]
                        ),
                        
                        Animated.parallel(
                            [
                                Animated.timing(
                                    this.state.line4, 
                                    {
                                        toValue: marginTo,
                                        duration: aniSpeed * 1.3,
                                        useNativeDriver: false,
                                    }
                                ),
                                Animated.timing(
                                    this.state.line4O, 
                                    {
                                        toValue: 1,
                                        duration: aniSpeed * 1.3,
                                        useNativeDriver: false,
                                    }
                                ),
                            ]
                        ),

                        Animated.parallel(
                            [
                                Animated.timing(
                                    this.state.line5, 
                                    {
                                        toValue: marginTo,
                                        duration: aniSpeed * 1.4,
                                        useNativeDriver: false,
                                    }
                                ),
                                Animated.timing(
                                    this.state.line5O, 
                                    {
                                        toValue: 1,
                                        duration: aniSpeed * 1.4,
                                        useNativeDriver: false,
                                    }
                                ),
                            ]
                        ),

                        Animated.parallel(
                            [
                                Animated.timing(
                                    this.state.line6, 
                                    {
                                        toValue: marginTo,
                                        duration: aniSpeed * 1.5,
                                        useNativeDriver: false,
                                    }
                                ),
                                Animated.timing(
                                    this.state.line6O, 
                                    {
                                        toValue: 1,
                                        duration: aniSpeed * 1.5,
                                        useNativeDriver: false,
                                    }
                                ),
                            ]
                        ),
                    ]
                ).start();
            },
            250
        );
    }

    async slideIn()
    {
        Animated.timing(
            this.state.slideLeft, 
            {
                toValue: 40,
                delay: 10,
                easing: Easing.in(Easing.elastic(0.8))
            }
        ).start();
    }

    async slideOut()
    {
        Animated.timing(
            this.state.slideLeft, 
            {
                toValue: -450,
                delay: 10,
                easing: Easing.out(Easing.elastic(0.8))
            }
        ).start();
    }

	render()
	{
		return (
			<Animated.View style={
				[
                    styles.cont,
                    this.props.style,
                    {
                        left: this.state.slideLeft
                    }
				]
			}>

                <Animated.View 
                    style={
                        [
                            styles.smallLine,
                            {
                                marginTop: this.state.line1,
                                opacity: this.state.line1O,
                            }
                        ]
                    } 
                />
                
                <Animated.View 
                    style={
                        [
                            styles.smallLine,
                            {
                                marginTop: this.state.line2,
                                opacity: this.state.line2O,
                            }
                        ]
                    } 
                />

                <Animated.View 
                    style={
                        [
                            styles.smallLine,
                            {
                                marginTop: this.state.line3,
                                opacity: this.state.line3O,
                            }
                        ]
                    } 
                />

                <Animated.View 
                    style={
                        [
                            styles.mediumLine,
                            {
                                marginTop: this.state.line4,
                                opacity: this.state.line4O,
                            }
                        ]
                    } 
                />

                <Animated.View 
                    style={
                        [
                            styles.largeLine,
                            {
                                marginTop: this.state.line5,
                                opacity: this.state.line5O,
                            }
                        ]
                    } 
                />

                <Animated.View 
                    style={
                        [
                            styles.mediumLine,
                            {
                                marginTop: this.state.line6,
                                opacity: this.state.line6O,
                            }
                        ]
                    } 
                />

			</Animated.View>
		);
	}
}

const styles = StyleSheet.create(getStyles());