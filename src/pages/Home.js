import React, {Component} from 'react';

import {
	StyleSheet,
    View,
	Animated,
	Easing,
	StatusBar,
} from 'react-native';

require('../classes/globals');

import {Button} from '@ui-kitten/components';

import Orientation from 'react-native-orientation-locker';

import Background      from '../components/Background';
import DefaultGradient from '../components/DefaultGradient';

import Lines from './home/Lines';

import LoginCont  from './login-signup-flow/LoginCont';
import SignupCont from './login-signup-flow/SignupCont';

function getStyles()
{
	let lrMargin = 27;

	return {
		mainCont:
		{
			flex: 1, 
			justifyContent: 'center', 
			alignItems: 'center'
		},
		pageCont:
		{
			position: 'absolute',
			width: '100%',
			top: 0,
			left: 0,
			bottom: 0,
			right: 0,
			flex: 1,
			alignItems: 'center',
			justifyContent: 'flex-end',
			paddingBottom: 90,
		},
		bottomSection:
		{
			width: '100%',
			paddingBottom: 23,
			paddingRight: 20,
			flex: 1,
			justifyContent: 'flex-end',
		},
		nextStep:
		{
			textAlign: 'right',
			paddingTop: 0
		},
		gradientCont:
		{
			flex: 1,
			width: '100%',
		},
		btnColumn:
		{
			width: '100%',
			flexDirection: 'column',
		},
		button:
		{
			marginLeft: lrMargin,
			marginRight: lrMargin,
		},
		inputCont:
		{
			width: '70%',
			paddingTop: 80
		},
		input: 
		{
			marginVertical: 2,
		},
		pageLoginSignupCont:
		{
			position: 'absolute',
			height: sh(84),
			left: 0,
			right: 0,
			alignItems: 'center',
		},
	};
}

export default class Home extends Component
{
	static navigationOptions = {headerShown: false};

	constructor(props)
	{
		super(props);

		this.state = {

			slideRight: new Animated.Value(0),

			gradientContAnit: new Animated.Value(0),
			btn1MT: new Animated.Value(10),
			btn2MT: new Animated.Value(10),
			btn1O: new Animated.Value(0),
			btn2O: new Animated.Value(0),

			lsContTop: new Animated.Value(sh(115)),

			isSignUp: false,

			currentSignUpTabIndex: -1,

			isSecondState: false,
		};

		this.events = props.screenProps.events;

		this.events.on(
			'animate-to-next-state',
			this.anmiateToFirstState.bind(this)
		);

		this.events.on(
			'logo-clicked',
			() =>
			{
				this.refs.lines.slideIn();
				this.slideBtnsIn();
			}
		);
	}

	componentDidMount() {Orientation.lockToPortrait()}

	async anmiateToFirstState(doBackFade = false)
	{
		this.setState({isSecondState: false})

		try
		{
			this.refs.lines.startAnimations();
		}
		catch (e) {}

		this.fadeButtonsIn();
		
		if (doBackFade) 
		{
			this.events.emit('fade-logo-in');

			this.refs.gradient.fadeOutBack();
		}

		this.refs.lines.slideIn();
		this.slideBtnsIn();

		Animated.parallel(
			[
				Animated.timing(
					this.state.gradientContAnit,
					{
						toValue: -100,
						duration: 300,
					}
				),
				Animated.timing(
					this.state.lsContTop,
					{
						toValue: sh(115),
						duration: 300,
					}
				),
			]
		).start();
	}

	async anmiateToSecondState()
	{
		this.refs.gradient.fadeInBack();
		this.events.emit('fade-logo-out');

		setTimeout(
			() =>
			{
				this.setState({isSecondState: true})
			},
			160
		);

		Animated.parallel(
			[
				Animated.timing(
					this.state.gradientContAnit,
					{
						toValue: -(sh(101)),
						duration: 300,
					}
				),
				Animated.timing(
					this.state.lsContTop,
					{
						toValue: sh(19.5),
						duration: 300,
					}
				),
			]
		).start();
	}

	async fadeButtonsIn()
	{
		let aniSpeed = 300,
			marginTo = 30;
		
		setTimeout(
			() =>
			{
				Animated.parallel(
					[
						Animated.parallel(
							[
								Animated.timing(
									this.state.btn1MT, 
									{
										toValue: marginTo,
										duration: aniSpeed,
										useNativeDriver: false,
									}
								),
								Animated.timing(
									this.state.btn1O, 
									{
										toValue: 1,
										duration: aniSpeed,
										useNativeDriver: false,
									}
								),
							]
						),
		
						Animated.parallel(
							[
								Animated.timing(
									this.state.btn2MT, 
									{
										toValue: marginTo,
										duration: aniSpeed * 1.3,
										useNativeDriver: false,
									}
								),
								Animated.timing(
									this.state.btn2O, 
									{
										toValue: 1,
										duration: aniSpeed * 1.3,
										useNativeDriver: false,
									}
								),
							]
						),
					]
				).start();
			},
			400
		);
	}

	async slideBtnsRight()
	{
		Animated.timing(
            this.state.slideRight, 
            {
                toValue: -750,
                delay: 10,
                easing: Easing.out(Easing.elastic(0.8))
            }
        ).start();
	}

	async slideBtnsIn()
	{
		Animated.timing(
            this.state.slideRight, 
            {
                toValue: 0,
                delay: 10,
                easing: Easing.in(Easing.elastic(0.8))
            }
        ).start();
	}

	gradientText()
	{
		if (!this.state.isSignUp) 
		{
			return 'LOGIN';
		}

		switch (this.state.currentSignUpTabIndex)
		{
			case 1: return 'SIGNUP - Sex';
			case 2: return 'SIGNUP - Age';
			case 3: return 'SIGNUP - Height';
			case 4: return 'SIGNUP - Weight';
			default: return 'SIGNUP';
		}
	}

	render()
	{
		return (
			<View style={styles.mainCont}>

				{this.state.isSecondState ? <StatusBar backgroundColor="#0061F7" /> : undefined}

				<Background />

				<Animated.View 
					style={
						[
							{width: '100%', height: '100%'},
							{
								top: this.state.gradientContAnit,
							}
						]
					}
				>

					<DefaultGradient
						ref="gradient"
						events={this.events}
						backText={this.gradientText()}
						onBackPressed={
							() =>
							{
								if (this.state.isSignUp)
								{
									if (this.refs['sign-up'].currentTab() == 0)
									{
										this.anmiateToFirstState(true);
									}
									else
									{
										this.refs['sign-up'].previousTab();
									}
								}
								else
								{
									this.anmiateToFirstState(true);
								}
							}
						}
					/>

				</Animated.View>

				<Lines ref="lines" />

				<View style={[styles.pageCont]}>

					<Animated.View 
						style={
							[
								styles.btnColumn,
								{
									marginRight: this.state.slideRight
								}
							]
						}
					>

						<Animated.View
							style={
								[
									styles.button,
									{
										marginTop: this.state.btn1MT,
										opacity: this.state.btn1O,
									}
								]
							} 
						>

							<Button 
								style={styles.button} 
								status='basic'
								onPress={
									() =>
									{
										this.refs.lines.slideOut();

										this.slideBtnsRight();

										setTimeout(
											() =>
											{
												this.setState({isSignUp: false});

												this.anmiateToSecondState();
											},
											400
										);
									}
								}
							>

								LOGIN

							</Button>

						</Animated.View>

						<Animated.View
							style={
								[
									styles.button,
									{
										marginTop: 10
									},
									{
										marginBottom: this.state.btn2MT,
										opacity: this.state.btn2O,
									}
								]
							} 
						>

							<Button 
								style={styles.button} 
								status='basic'
								onPress={
									() =>
									{
										this.refs.lines.slideOut();

										this.slideBtnsRight();

										setTimeout(
											async () =>
											{
												this.setState({isSignUp: true});

												this.anmiateToSecondState();
											},
											442
										);
									}
								}
							>

								SIGNUP

							</Button>

						</Animated.View>

					</Animated.View>

					<Animated.View style={
							[
								styles.pageLoginSignupCont,
								{
									top: this.state.lsContTop
								}
							]
						}
					>
						{this.state.isSignUp ? <SignupCont 
							ref="sign-up" 
							events={this.events}
							navi={this.props.navigation}
							onIndexChanged={
								(index) =>
								{
									this.setState({currentSignUpTabIndex: index});
								}
							}
						/> : <LoginCont 
							events={this.events} 
							navi={this.props.navigation}
						/>}

					</Animated.View>

				</View>

			</View>
		);
	}
}

const styles = StyleSheet.create(getStyles());