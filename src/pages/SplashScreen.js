import React, {Component} from 'react';

import {
	StyleSheet,
	View,
	Animated,
	Easing,
	StatusBar
} from 'react-native';

import TxtLogo from '../components/TxtLogo';

function getStyles()
{
	return {
		container:
		{
			position: 'absolute',
			left: 0,
			right: 0,
			top: 0,
			bottom: 0,
			alignItems: 'center',
		},
	};
}

export default class SplashScreen extends Component
{
	constructor (props)
	{
		super(props);

		// setTimeout(
		// 	() =>
		// 	{
		// 		this.fadeout();
		// 	},
		// 	this.props.timeout || 1500
		// );

		this.events = props.events;

		this.state = {
			fadeAnim: new Animated.Value(1),
			logoScale: new Animated.Value(0),
			logoOpacity: new Animated.Value(1),
			logoTop: new Animated.Value(250),
			loaded: false
		};

		setTimeout(() => this.animateLogoIn(), 200);

		this.events.on(
			'fade-logo-in',
			() => this.fadeInLogo()
		);

		this.events.on(
			'fade-logo-out',
			() => this.fadeOutLogo()
		);
	}

	animateLogoIn()
	{
		Animated.timing(
			this.state.logoScale, 
			{
				toValue: 1,
				duration: 600,
				easing: Easing.elastic(1.5)
			}
		).start(
			() =>
			{
				setTimeout(() => this.animateOut(), 1500);
			}
		);
	}

	animateOut()
	{
		let aniSeed = 700;

		this.setState({isInSecondState: true});

		setTimeout(() => this.props.fadeOutToNextStep(), 300);

		Animated.parallel(
			[
				Animated.timing(
					this.state.logoScale, 
					{
						toValue: 0.7,
						duration: aniSeed,
						useNativeDriver: false,
						// easing: Easing.out
					}
				),
				Animated.timing(
					this.state.logoOpacity, 
					{
						toValue: 0.7,
						duration: aniSeed,
						useNativeDriver: false,
						easing: Easing.elastic(1.5)
					}
				),
				Animated.timing(
					this.state.logoTop, 
					{
						toValue: 10,
						duration: aniSeed,
						useNativeDriver: false,
						// easing: Easing.out
					}
				),
			]
		).start();
	}

	async fadeInLogo()
	{
		let aniSeed = 700;

		Animated.parallel(
			[
				Animated.timing(
					this.state.logoOpacity, 
					{
						toValue: 0.7,
						duration: aniSeed,
						useNativeDriver: false,
						easing: Easing.elastic(1.5)
					}
				),
				Animated.timing(
					this.state.logoScale, 
					{
						toValue: 0.7,
						duration: aniSeed,
						useNativeDriver: false,
						easing: Easing.elastic(1.5)
					}
				),
			]
		).start();
	}

	async fadeOutLogo()
	{
		let aniSeed = 700;

		Animated.parallel(
			[
				Animated.timing(
					this.state.logoOpacity, 
					{
						toValue: 0,
						duration: aniSeed,
						useNativeDriver: false,
						easing: Easing.elastic(1.5)
					}
				),
				Animated.timing(
					this.state.logoScale, 
					{
						toValue: 0.1,
						duration: aniSeed,
						useNativeDriver: false,
						easing: Easing.elastic(1.5)
					}
				),
			]
		).start();
	}

	fadeout()
	{
		Animated.timing(
			this.state.fadeAnim,
			{
				toValue: 0,
				duration: 700,
				useNativeDriver: true,
			}
		).start();

		return;

		setTimeout(
			() =>
			{
				if (this.props.onSplashClosed)
				{
					this.props.onSplashClosed();
				}

				this.setState({loaded: true});
			},
			680
		);
	}

	render()
	{
		if (this.state.loaded) return <View />;

		return (
			<Animated.View style={
				[
					styles.container, 
					{
						opacity: this.state.fadeAnim, 
						height: !this.state.loaded ? '100%' : '0%',
					}
				]
			}>

				<StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.8)" />

				<Animated.View 
					style={
						{
							top: this.state.logoTop,
							opacity: this.state.logoOpacity,
							transform: [
								{
									scale : this.state.logoScale
								},
							],
						}
					}
				>

					<TxtLogo onPress={() => this.events.emit('logo-clicked')} />

				</Animated.View>

			</Animated.View>
		);
	}
}

const styles = StyleSheet.create(getStyles());