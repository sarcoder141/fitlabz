/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';

import { EvaIconsPack } from '@ui-kitten/eva-icons';

import RootNavigator from './RootNavigator';

import SplashScreen from './pages/SplashScreen';

const showSplash = true;

import Consts from './classes/Consts';
import Events from './classes/Events';

const cnts = new Consts();
const evts = new Events();

export default () => (
  <ApplicationProvider {...eva} theme={eva.light}>
    
    <IconRegistry icons={EvaIconsPack} />
    
    <RootNavigator screenProps={{consts: cnts, events: evts}}/>

      {
        showSplash ? (
          <SplashScreen
            events={evts}
            fadeOutToNextStep={
            () =>
              {
                evts.emit('animate-to-next-state');
            }
          }
        />
      ) : null}

  </ApplicationProvider>
);