import React, {Component} from 'react';

import {
	StyleSheet, 
	View, 
	Dimensions, 
	Animated, 
	TouchableOpacity, 
	Keyboard
} from 'react-native';

import {
	Text
} from '@ui-kitten/components';

import Icon from './Icon';

import LinearGradient from 'react-native-linear-gradient';

require('../classes/globals');

const styles = StyleSheet.create(
	{
		linearGradient: 
		{
			flex: 1,
			width: '100%',
			position: 'absolute',
			bottom: 0,
			paddingTop: '15%',
			height: '100%',
		},
		bottomSection:
		{
			position: 'absolute',
			height: 130,
			left: 0,
			right: 0,
			backgroundColor:  '#0061F7',
			top: Dimensions.get('window').height + 25,
			borderBottomLeftRadius:40
		},
		bottomSectionPage:
		{
			position: 'absolute',
			width: '100%',
			left: 0,
			right: 0,
			bottom: -(sh(101)),
			height: sh(101),
			backgroundColor: 'white',
			flex: 1
		},
		headerSection: 
		{
			position: 'absolute',
			flexDirection: 'column',
			justifyContent: 'space-between',
			height: 70,
			left: 0,
			right: 0,
			bottom: 25,
			paddingLeft: 26,
			overflow: 'hidden'
		},
		label:
		{
			fontSize: 18,
			color: 'white', 
			fontWeight: 'bold'
		},
		icn:
		{
			marginLeft: -5
		},
		bgClickable: 
		{
			position: 'absolute',
			left: 0,
			right: 0,
			bottom: 0,
			top: 0,
		},
	}
);

export default class DevaultGradient extends Component
{
	constructor(props)
	{
		super(props);

		this.state = {
			backA: new Animated.Value(-50),
			backO: new Animated.Value(0),

			backIA: new Animated.Value(50),
			backIO: new Animated.Value(0),

			showBack: false,
		};

		this.props.events.on(
			'default-gradient-hide-back',
			() =>
			{
				this.hideBackArrow();
			}
		);

		this.props.events.on(
			'default-gradient-show-back',
			() =>
			{
				this.showBackArrow();
			}
		);
	}

	async fadeOutBack()
	{
		let to = 370;

		Animated.parallel(
			[
				Animated.timing(
					this.state.backA,
					{
						duration: to,
						toValue: -50,
						useNativeDriver: false,
					}
				),
				Animated.timing(
					this.state.backO,
					{
						duration: to,
						toValue: 0,
						useNativeDriver: false,
					}
				),
				Animated.timing(
					this.state.backIA,
					{
						duration: to,
						toValue: 50,
						useNativeDriver: false,
					}
				),
				Animated.timing(
					this.state.backIO,
					{
						duration: to,
						toValue: 0,
						useNativeDriver: false,
					}
				),
			]
		).start();
	}

	async fadeInBack()
	{
		let to = 170;

		this.setState({showBack: true})

		setTimeout(
			() =>
			{
				Animated.parallel(
					[
						Animated.timing(
							this.state.backA,
							{
								duration: to,
								toValue: 0,
								useNativeDriver: false,
							}
						),
						Animated.timing(
							this.state.backO,
							{
								duration: to,
								toValue: 1,
								useNativeDriver: false,
							}
						),
						Animated.timing(
							this.state.backIA,
							{
								duration: to,
								toValue: 0,
								useNativeDriver: false,
							}
						),
						Animated.timing(
							this.state.backIO,
							{
								duration: to,
								toValue: 1,
								useNativeDriver: false,
							}
						),
					]
				).start();
			},
			250
		);
	}

	async hideBackArrow()
	{
		let to = 370;

		Animated.parallel(
			[
				Animated.timing(
					this.state.backIA,
					{
						duration: to,
						toValue: 50,
						useNativeDriver: false,
					}
				),
				Animated.timing(
					this.state.backIO,
					{
						duration: to,
						toValue: 0,
						useNativeDriver: false,
					}
				),
			]
		).start();

		setTimeout(
			() => this.setState({showBack: false}),
			to
		);
	}

	async showBackArrow()
	{
		let to = 370;

		this.setState({showBack: true});

		Animated.parallel(
			[
				Animated.timing(
					this.state.backIA,
					{
						duration: to,
						toValue: 0,
						useNativeDriver: false,
					}
				),
				Animated.timing(
					this.state.backIO,
					{
						duration: to,
						toValue: 1,
						useNativeDriver: false,
					}
				),
			]
		).start();
	}

	render()
	{
		return (
			<LinearGradient
				style={[styles.linearGradient, this.props.style]}
				colors={
					[
						'transparent',
						'#0061F720',
						'#0061F770',
						'#0061F7'
					]
				} 
			>
				<View style={styles.bottomSectionPage}>

					<TouchableOpacity style={styles.bgClickable} onPress={() => Keyboard.dismiss()}/>

				</View>

				<View style={styles.bottomSection}>

					<View style={styles.headerSection}>

						<Animated.View
							style={
								{
									marginLeft: this.state.backIA,
									opacity: this.state.backIO
								}
							}
						>

							<Icon
								style={styles.icn}
								size={30} 
								fill='#fff' 
								name='arrow-back-outline'
								enabled={this.state.showBack}
								onPress={
									() =>
									{
										if (this.props.onBackPressed)
										{
											this.props.onBackPressed();
										}
									}
								}
							/>

						</Animated.View>

						<TouchableOpacity
							style={[styles.label]}
							onPress={
								() =>
								{
									this.props.events.emit('title-clicked');

									this.hideBackArrow();
								}
							}
						>

							<Animated.Text 
								style={
									[
										styles.label,
										{
											marginLeft: this.state.backA,
											opacity: this.state.backO
										}
									]
								}
							>
								
								{this.props.backText}

							</Animated.Text>

						</TouchableOpacity>

					</View>

				</View>
				
			</LinearGradient>
		);
	}
}