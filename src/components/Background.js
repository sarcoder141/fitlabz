import React, {Component} from 'react';

import {
	StyleSheet,
	Image,
} from 'react-native';

function getStyles()
{
	return {
		container:
		{
			position: 'absolute',
			left: 0,
			right: 0,
			top: 0,
			bottom: 0,
		}
	};
}

export default class Background extends Component
{
	render = () => <Image
		style={[styles.container]}
		source={require('../assets/bg-main.jpg')}
		blurRadius={this.props.blurRadius}
	/>
}

const styles = StyleSheet.create(getStyles());