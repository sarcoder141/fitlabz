import React, {Component} from 'react';

import {StyleSheet, View, TouchableOpacity} from 'react-native';

import { Text } from '@ui-kitten/components';

import OutlinedText from './text/OutlinedText';

require('../classes/globals');

function getStyles()
{
	let fontSize     = 80;
	let fontBSize    = 80;

	return {
		cont:
		{
			width: '100%',
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center'
		},
		row: 
		{
			width: '100%',
			flexDirection: 'row'
		},
		col:
		{	
			width: fontSize * 3.3,
			paddingLeft: 0,
		},
		fnt:
		{
			fontStyle: 'italic',
			fontWeight: 'bold',
			letterSpacing: 3
		},
		fit:
		{
			fontSize: fontSize * 1.1,
			color: 'white',
		},
		labzCont: 
		{
			flexDirection: 'row-reverse',
			height: fontBSize * 1.1,
		},
	}
}

const styles = StyleSheet.create(getStyles());

export default class TxtLogo extends Component
{
	render()
	{
		return (
			<TouchableOpacity 
				style={[styles.cont, this.props.style, {zIndex: 10000000}]}
				onPress={
					() => 
					{
						if (this.props.onPress)
						{
							this.props.onPress();
						}
					}
				}
			>

				<View style={styles.col}>

					<View style={[styles.col]}>

						<Text style={[styles.fit, styles.fnt]}>Fit </Text>

					</View>

					<View style={[styles.col, styles.labzCont]}>

						<OutlinedText rightPadding={37} text="Labz" />

					</View>

				</View>

			</TouchableOpacity>
		);
	}
}