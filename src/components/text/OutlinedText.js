import React, {Component} from 'react';

import {
	StyleSheet,
	View,
	Text,
} from 'react-native';

var FONT_RIGHT = 50;
var FONT_TOP   = -30;
var OFFSET     = 1.6;

function getStyles()
{
	let fontSize  = 80;
	let fontBSize = 80;
    
    return {
		fnt:
		{
			fontStyle: 'italic',
			fontWeight: 'bold',
			letterSpacing: 3
		},
		fit:
		{
			fontSize: fontSize * 1.1,
			color: 'white',
			textShadowOffset:
			{
				width: 2,
				height: 2
			},
		},
        txt:
		{
			position: 'absolute',
			color: 'black',
			fontSize: fontBSize,
			right: FONT_RIGHT,
			top: FONT_TOP,
		},
		txtShaddow:
		{
			position: 'absolute',
			color: 'white',
			fontSize: fontBSize,
		},
    };
}

const styles = StyleSheet.create(getStyles());

export default class DevaultGradient extends Component
{
	render()
	{
		return (
            <View style={[this.props.style, {padding: 1, flex: 1}]}>

				{/* TEXT SHADOW TOP */}

				<Text style={
					[
						styles.txtShaddow, styles.fnt, 
						{
							right: this.props.rightPadding ? this.props.rightPadding        : FONT_RIGHT,
							top: this.props.topPadding     ? this.props.topPadding - OFFSET : FONT_TOP - OFFSET,
						}
					]
				}>
					
					{ this.props.text || 'na' }
				
				</Text>

				{/* TEXT SHADOW BOTTOM */}

                <Text style={
					[
						styles.txtShaddow, styles.fnt, 
						{
							right: this.props.rightPadding ? this.props.rightPadding        : FONT_RIGHT,
							top: this.props.topPadding     ? this.props.topPadding + OFFSET : FONT_TOP + OFFSET,
						}
					]
				}>
					
					{ this.props.text || 'na' }
					
				</Text>

				{/* TEXT SHADOW LEFT */}

				<Text style={
					[
						styles.txtShaddow, styles.fnt, 
						{
							right: this.props.rightPadding ? this.props.rightPadding + OFFSET: FONT_RIGHT + OFFSET,
							top: FONT_TOP,
						}
					]
				}>
					
					{ this.props.text || 'na' }
					
				</Text>

				{/* TEXT SHADOW RIGHT */}

				<Text style={
					[
						styles.txtShaddow, 
						styles.fnt, 
						{
							right: this.props.rightPadding ? this.props.rightPadding - OFFSET: FONT_RIGHT - OFFSET,
							top: FONT_TOP,
						}
					]
				}>
					
					{ this.props.text || 'na' }
					
				</Text>

                <Text style={[styles.txt, styles.fnt, {right: this.props.rightPadding + OFFSET? this.props.rightPadding : FONT_RIGHT}]}>
					
					{ this.props.text || 'na' }
					
				</Text>

            </View>
		);
	}
}