
import React, {Component} from 'react';

import {TouchableWithoutFeedback, View} from 'react-native';

import {Input, Icon} from '@ui-kitten/components';

const AlertIcon = (props) => (
	<Icon {...props} name='alert-circle-outline'/>
);

export default class TextInput extends Component {
  
    constructor(props)
    {
        super(props);

        this.state = {
            [this.props.bModel]: [this.props.parent.state[this.props.bModel]],
            secureTextEntry: this.props.isSecureText,
            isSecureText: this.props.isSecureText,
            caption: this.props.caption,
            placeholder: this.props.placeholder,
            label: this.props.label,
        };
    }

    toggleSecureEntry()
    {
        this.setState(
            {
                secureTextEntry: !this.state.secureTextEntry
            }
        );
    }

    render()
    {
        const renderIcon = (props) => {
            
            if (!this.state.isSecureText) return <View />;
            
            return (
                <TouchableWithoutFeedback onPress={this.toggleSecureEntry.bind(this)}>
        
                    <Icon {...props} name={this.state.secureTextEntry ? 'eye-off' : 'eye'}/>
        
                </TouchableWithoutFeedback>
            );
        };

        return (
            <Input
                style={this.props.style}
                value={this.props.parent[this.props.bModel]}
                label={this.state.label}
                placeholder={this.state.placeholder}
                caption={this.state.caption}
                accessoryRight={renderIcon}
                captionIcon={this.state.caption == undefined ? undefined : AlertIcon}
                secureTextEntry={this.state.secureTextEntry}
                disabled={this.props.disabled}
                onChangeText={
                    (nextValue) => 
                    {
                        // this.setState(
                        //     {
                        //         value: nextValue
                        //     }
                        // );

                        this.props.parent.setState(
                            {
                                [this.props.bModel]: nextValue,
                            }
                        );
                    }
                }
            />
        );
    }
};