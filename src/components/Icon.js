import React, {Component} from 'react';

import {TouchableOpacity} from 'react-native';

import {Icon} from '@ui-kitten/components';

export default class Icn extends Component
{   
    render()
	{
		if (this.props.onPress)
		{
			return (
				<TouchableOpacity
					enabled={this.props.enabled || true}
					onPress={
						() =>
						{
							if (this.props.onPress)
							{
								this.props.onPress();
							}
						}
					}
				>
					<Icon 
						style={
							[
								{
									width: this.props.size || 25, 
									height: this.props.size || 25
								},
								this.props.style
							]
						} 
						fill={this.props.fill || 'white'}
						name={this.props.name || 'star'}
					/>
				</TouchableOpacity>
			);
		}

		return <Icon 
			style={
				[
					{
						width: this.props.size || 25, 
						height: this.props.size || 25
					},
					this.props.style
				]
			} 
			fill={this.props.fill || 'white'}
			name={this.props.name || 'star'}
		/>;
	}
}