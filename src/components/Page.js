import React, {Component} from 'react';

import {
	StyleSheet,
	View,
	Platform,
} from 'react-native';

// import Theme      from '../Theme';
import Background from './Background';

const styles = StyleSheet.create(
	{
		page:
		{
			position: 'absolute',
			left: 0,
			right: 0,
			top: 0,
			bottom: 0,
		},
		pageCont:
		{
			// top: Theme.pageHeader.height,
			left: 0,
			right: 0,
			bottom: 0,
			position: 'absolute',
		},
		whiteOverlay:
		{
			position: 'absolute',
			left: 0,
			right: 0,
			// top: Theme.pageHeader.height,
			bottom: 0,
			// backgroundColor: Theme.whiteBgOverlayColor
		}
	}
);

export default class Page extends Component
{
	renderWhiteOverlay()
	{
		if (!this.props.showWhiteOverlay) return;

		return <View style={styles.whiteOverlay} />;
	}

	render()
	{
		return (
			<View style={[styles.page, this.props.style]}>

				<Background blurRadius={this.props.blurRadius} />

				{this.renderWhiteOverlay()}

				<View style={[styles.pageCont, {alignItems: this.props.centerContent ? 'center' : 'flex-start'}]}>

					{this.props.children}

				</View>

			</View>
		);
	}
}